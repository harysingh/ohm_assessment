from tests import OhmTestCase

class CommunityPageTest(OhmTestCase):

    def test_url_community(self):
        response = self.client.get('/community')
        self.assert_status(response, 200)
    