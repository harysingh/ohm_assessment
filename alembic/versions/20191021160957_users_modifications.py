"""Users modifications

Revision ID: 3f172b4e8fbd
Revises: 00000000
Create Date: 2019-10-21 16:09:57.917968

"""

# revision identifiers, used by Alembic.
revision = '3f172b4e8fbd'
down_revision = '00000000'

from alembic import op
import sqlalchemy as sa


def upgrade():
    op.execute('''UPDATE user SET point_balance=5000 WHERE user_id=1''')
    op.execute('''INSERT INTO rel_user (user_id, rel_lookup, attribute) VALUES (3, 'LOCATION', 'USA')''')
    op.execute('''UPDATE user SET tier='Silver' WHERE user_id=3''')


def downgrade():
    op.execute('''UPDATE user SET point_balance=0 WHERE user_id=1''')
    op.execute('''DELETE FROM rel_user WHERE user_id=2''')
    op.execute(''''UPDATE user SET tier='Carbon' WHERE user_id=3''')
